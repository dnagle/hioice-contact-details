<?php
/**
 * Plugin Name: HIOICE Contact Details
 * Description: Adds Contact Detail Widgets to WordPress Theme. 
 * Author: Dan Nagle
 * Text Domain: hioice-contact
 * Domain Path: /languages/
 * Version: 1.0.0
 * License: GPLv2
 * Bitbucket Plugin URI: https://bitbucket.org/dnagle/hioice-contact-details
 *
 * @package WordPress
 * @author Dan Nagle
 */

if ( !defined( 'HIOICE_CD_VERSION' ) ) {
	define( 'HIOICE_CD_VERSION', '1.0.0' ); // Plugin version
}
if ( !defined( 'HIOICE_CD_DIR' ) ) {
	define( 'HIOICE_CD_DIR', dirname( __FILE__ ) ); // Plugin directory
}
if ( !defined( 'HIOICE_CD_URL' ) ) {
	define( 'HIOICE_CD_URL', plugin_dir_url( __FILE__ ) ); // Plugin url
}
if ( !defined( 'HIOICE_CD_DOMAIN' ) ) {
	define( 'HIOICE_CD_DOMAIN', 'hioice-contact' ); // Text Domain
}
if ( !defined( 'HIOICE_CD_SECTION' ) ) {
	define( 'HIOICE_CD_SECTION', 'hioice_contact_details' ); // Text Domain
}

/**
 * Load Text Domain
 * This gets the plugin ready for translation
 * 
 * @package HIOICE Contact Details
 * @since 1.0.0
 */
function hioice_contact_load_textdomain() {
	load_plugin_textdomain( HIOICE_CD_DOMAIN, false, HIOICE_CD_DIR . '/languages/' );
}
add_action('plugins_loaded', 'hioice_contact_load_textdomain');

/**
 * Class loader for library that parses and validates international phone numbers.
 */
function hioice_contact_autoloader($class) {
	if ((strpos($class, 'libphonenumber') !== 0) && (strpos($class, 'Giggsey') !== 0)) {
		return;
	}
	$directory = str_replace('\\', '/', $class);
	$filename = HIOICE_CD_DIR . '/lib/' . $directory . '.php';
	include_once $filename;
}
spl_autoload_register('hioice_contact_autoloader');

require_once HIOICE_CD_DIR . '/includes/hioice-contact-customizer.php';

require_once HIOICE_CD_DIR . '/includes/hioice-contact-shortcodes.php';
