<?php

add_shortcode( 'hioice-contact-details', 'hioice_sc_contact_details' );

function hioice_sc_icon_img_tag( $icon, $class ) {

	$icons = array( 'email', 'fax', 'phone' );

//	if ( empty($icon) || !array_key_exists( $icon, $icons )) {
//		return;
//	}

	$icons_url = HIOICE_CD_URL . '/images/' . $icon;

	$img_tag = '<img srcset="'. $icons_url . '@2x.png 2x, '. $icons_url . '@3x.png 3x"';
	$img_tag .= ' src="' . $icons_url . '.png" alt=""';
	if ( ! empty($class) ) {
		$img_tag .= ' class="contact-icon ' . $class . '"';
	}
	$img_tag .= ' width="32" height="32" />';

	return $img_tag;

}

function hioice_sc_contact_details( $attr, $content ) {

	$company_name = get_theme_mod( 'hioice_contact_company_name' );
	$company_address_1 = get_theme_mod( 'hioice_contact_address_1' );
	$company_address_2 = get_theme_mod( 'hioice_contact_address_2' );
	$company_address_3 = get_theme_mod( 'hioice_contact_address_3' );
	$company_locality = get_theme_mod( 'hioice_contact_locality' );
	$company_postcode = get_theme_mod( 'hioice_contact_postcode' );
	$company_phone_number = get_theme_mod( 'hioice_contact_phone_number' );
	$company_phone_link = get_theme_mod( 'hioice_contact_phone_link' );
	$company_phone_title = get_theme_mod( 'hioice_contact_phone_cta' );
	$company_fax_number = get_theme_mod( 'hioice_contact_fax_number' );
	$company_fax_title = get_theme_mod( 'hioice_contact_fax_cta' );
	$company_email = get_theme_mod( 'hioice_contact_email' );
	$company_email_title = get_theme_mod( 'hioice_contact_email_cta' );

	$output = '<div itemscope itemtype="http://schema.org/LocalBusiness">';

	if ( ! empty( $company_name ) ) {
		$output .= '<span itemprop="legalName">' . $company_name . '</span><br/>';
	}
	if ( ! empty( $company_address_1 ) ) {
		$output .= '<span itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">';
		$output .= '<span itemprop="streetAddress">';
		if ( ! empty( $company_address_1 ) ) {
			$output .= $company_address_1 . '<br/>';
		}
		if ( ! empty( $company_address_2 ) ) {
			$output .= $company_address_2 . '<br/>';
		}
		if ( ! empty( $company_address_3 ) ) {
			$output .= $company_address_3 . '<br/>';
		}
		$output .= '</span>';
		if ( ! empty( $company_locality ) ) {
			$output .= '<span itemprop="addressLocality">' . $company_locality . '</span><br/>';
		}
		if ( ! empty( $company_postcode ) ) {
			$output .= '<span itemprop="postalCode">' . $company_postcode . '</span><br/>';
		}
		$output .= '</span>';
	}
	if ( ! empty( $company_phone_number ) || ! empty( $company_fax_number ) || ! empty( $company_email ) ) {
		$output .= '<ul class="contact-list">';
		/* Phone Number */
		if ( ! empty( $company_phone_number ) ) {
			$output .= '<li class="contact-list-item">' . hioice_sc_icon_img_tag('phone', '');
			$output .= '<a href="tel:' . $company_phone_link . '"';
			if ( ! empty( $company_phone_title ) ) {
				$output .= ' title="' . $company_phone_title . '"';
			}
			$output .= ' class="a-phone-link" itemprop="telephone">' . $company_phone_number . '</a>';
			$output .= '</li>';
		}
		/* Fax Number */
		if ( ! empty( $company_fax_number ) ) {
			$output .= '<li class="contact-list-item"><span class="item-wrap">' . hioice_sc_icon_img_tag('fax', '');
			$output .= '<span itemprop="faxNumber">' . $company_fax_number . '</span>';
			$output .= '</span></li>';
		}
		/* Email Address */
		if ( ! empty( $company_email ) ) {
			$output .= '<li class="contact-list-item">' . hioice_sc_icon_img_tag('email', '');
			if ( is_email($company_email) ) {
				$output .= '<a href="mailto:' . antispambot($company_email) . '"';
				if ( ! empty( $company_email_title ) ) {
					$output .= ' title="' . $company_email_title . '"';
				}
				$output .= ' class="a-email-link" itemprop="email">' . antispambot($company_email) . '</a>';
			} else {
				$output .= '<span itemprop="email">' . $company_email . '</span>';
			}
			$output .= '</li>';
		}
		$output .= '</ul>';
	}
	$output .= '</div>';

	
	return $output;

}

function hioice_contact_address_str() {
	$address_fields = array( 'hioice_contact_company_name', 'hioice_contact_address_1', 'hioice_contact_address_2', 'hioice_contact_address_3', 'hioice_contact_locality', 'hioice_contact_postcode', 'hioice_contact_country' );
	$address_str = '';
	$separator = '';

	foreach ($address_fields as $field) {
		$value = trim( get_theme_mod($field) );
		if (! empty($address_str)) {
			$separator = ',';
		}
		if (! empty($value)) {
			$address_str .= $separator . $value;
		}
	}

	return rfc3986UrlEncode($address_str);
}

function rfc3986UrlEncode($string) {
    $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%2B', '%24', '%2C', '%2F', '%3F', '%25', '%23', '%5B', '%5D');
    $replacements = array("!", "*", "'", "(", ")", ";", ":", "@", "&", "=", "+", "$", ",", "/", "?", "%", "#", "[", "]");
    return str_replace($entities, $replacements, urlencode($string));
}
