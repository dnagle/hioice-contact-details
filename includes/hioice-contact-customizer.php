<?php

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

function hioice_parse_phone_number($phone_number) {
	if (!empty($phone_number)) {
		$phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
		$phoneNumberObject = $phoneNumberUtil->parse($phone_number, null);
		if ($phoneNumberUtil->isPossibleNumber($phoneNumberObject)) {
			// e.g. result =  "+35318765432"
			$result = $phoneNumberUtil->format($phoneNumberObject, \libphonenumber\PhoneNumberFormat::E164);
		}
	}
	return $result;
}

/**
 * Register settings and controls with the Customizer.
 *
 * @since 1.0.0
 *
 * @param WP_Customize_Manager $wp_customize Customizer object.
 */
add_action( 'customize_register', 'hioice_contact_customizer_register' );
function hioice_contact_customizer_register( $wp_customize ) {

	$wp_customize->add_section( HIOICE_CD_SECTION, array(
		'title' => __( 'Company Details', HIOICE_CD_DOMAIN ),
		'description' => __('Add company details to your website', HIOICE_CD_DOMAIN ),
		'capability' => 'edit_theme_options'
	));

	/* Company Name */
	$wp_customize->add_setting( 'hioice_contact_company_name', array(
		'default'           => get_bloginfo('name'),
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'snp_contact_company_name', array(
		'label'      => __( 'Company Name', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_company_name',
		'type'       => 'text',
	) ) );

	/* Company Address 1 */
	$wp_customize->add_setting( 'hioice_contact_address_1', array(
		'default'           => '',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_address_1', array(
		'label'      => __( 'Address Line 1', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_address_1',
		'type'       => 'text',
	) ) );

	/* Company Address 2 */
	$wp_customize->add_setting( 'hioice_contact_address_2', array(
		'default'           => '',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_address_2', array(
		'label'      => __( 'Address Line 2', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_address_2',
		'type'       => 'text',
	) ) );

	/* Company Address 3 */
	$wp_customize->add_setting( 'hioice_contact_address_3', array(
		'default'           => '',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_address_3', array(
		'label'      => __( 'Address Line 3', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_address_3',
		'type'       => 'text',
	) ) );

	/* Company Address Locality */
	$wp_customize->add_setting( 'hioice_contact_locality', array(
		'default'           => '',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_locality', array(
		'label'      => __( 'Locality', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_locality',
		'type'       => 'text',
	) ) );

	/* Company Address Postcode */
	$wp_customize->add_setting( 'hioice_contact_postcode', array(
		'default'           => '',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_postcode', array(
		'label'      => __( 'Postcode', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_postcode',
		'type'       => 'text',
	) ) );

	/* Company Address Country */
	$wp_customize->add_setting( 'hioice_contact_country', array(
		'default'           => '',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_country', array(
		'label'      => __( 'Country', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_country',
		'type'       => 'text',
	) ) );

	/* Company Phone Number */
	$wp_customize->add_setting( 'hioice_contact_phone_number', array(
		'default'           => '#',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_phone_number', array(
		'label'      => __( 'Phone Number', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_phone_number',
		'type'       => 'text',
	) ) );

	/* Company Phone Number Link */
	$wp_customize->add_setting( 'hioice_contact_phone_link', array(
		'default'           => hioice_parse_phone_number( get_theme_mod( 'hioice_contact_phone_number' ) ),
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_phone_link', array(
		'label'      => __( 'Phone Number Link', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_phone_link',
		'type'       => 'text',
	) ) );

	/* Company Phone Number Call To Action */
	$wp_customize->add_setting( 'hioice_contact_phone_cta', array(
		'default'           => 'Call Now',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'snp_contact_phone_cta', array(
		'label'      => __( 'Phone Number CTA', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_phone_cta',
		'type'       => 'text',
	) ) );

	/* Company Fax Number */
	$wp_customize->add_setting( 'hioice_contact_fax_number', array(
		'default'           => '#',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_fax_number', array(
		'label'      => __( 'Fax Number', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_fax_number',
		'type'       => 'text',
	) ) );

	/* Company Fax Number Call To Action */
	$wp_customize->add_setting( 'hioice_contact_fax_cta', array(
		'default'           => 'Fax',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_fax_cta', array(
		'label'      => __( 'Fax Number CTA', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_fax_cta',
		'type'       => 'text',
	) ) );

	/* Company Email Address */
	$wp_customize->add_setting( 'hioice_contact_email', array(
		'default'           => get_bloginfo('admin_email'),
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_email', array(
		'label'      => __( 'Email Address', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_email',
		'type'       => 'text',
	) ) );

	/* Company Email Call To Action */
	$wp_customize->add_setting( 'hioice_contact_email_cta', array(
		'default'           => 'Email Me',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_email_cta', array(
		'label'      => __( 'Email CTA', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_email_cta',
		'type'       => 'text',
	) ) );

	/* Company Registration */
	$wp_customize->add_setting( 'hioice_contact_company_reg', array(
		'default'           => '',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_company_reg', array(
		'label'      => __( 'Company Registration', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_company_reg',
		'type'       => 'text',
	) ) );

	/* Company Tax Registration */
	$wp_customize->add_setting( 'hioice_contact_tax_reg', array(
		'default'           => '',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_tax_reg', array(
		'label'      => __( 'Tax Registration', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_tax_reg',
		'type'       => 'text',
	) ) );

	/* Google Maps API Key */
	$wp_customize->add_setting( 'hioice_contact_gmaps_api_key', array(
		'default'           => 'YOUR_API_KEY',
	) );

	$wp_customize->add_control( new WP_Customize_Control(
		$wp_customize, 'hioice_contact_gmaps_api_key', array(
		'description' => "Be sure to replace ‘YOUR_API_KEY’ with <a href='https://developers.google.com/maps/documentation/geocoding/start#get-a-key'>your actual API key</a>.",
		'label'      => __( 'Google Maps API Key', HIOICE_CD_DOMAIN ),
		'section'    => HIOICE_CD_SECTION,
		'settings'   => 'hioice_contact_gmaps_api_key',
		'type'       => 'text',
	) ) );

}